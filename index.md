
# Índice
## UD01 - Introdución as Redes Locais


*RA1. Recoñece a estrutura das redes de datos, e identifica os seus elementos e os seus principios de funcionamento.
-CA1.1. Identificáronse os factores que impulsan a continua expansión e evolución das redes de datos.
-CA1.2. Describíronse as arquitecturas de rede e os seus niveis.
-CA1.3. Describiuse o funcionamento das pilas de protocolos nas arquitecturas de rede.
-CA1.4. Recoñecéronse os tipos de rede e as súas topoloxías.
-CA1.5. Presentáronse e describíronse os elementos funcionais, físicos e lóxicos das redes de datos.
-CA1.6. Diferenciáronse os medios de transmisión utilizados nas redes.
-CA1.7. Describiuse o concepto de protocolo de comunicación.
-CA1.8. Diferenciáronse os dispositivos de interconexión de redes atendendo ao nivel funcional en que se encadren.

#### [1.1. Introdución](UD01/1.1.Introduccion.md)
#### [1.2. Topoloxías](UD01/1.2.Topoloxias.md)
#### [1.3. Clasificación das redes](UD01/1.3.Clasificacion_redes.md)
#### [1.4. Modos e unidades de envío de información](UD01/1.4.Modos_unidades_envio.md)
#### [1.5. Sistemas de numeración](UD01/1.5.Sistemas_de_numeracion.md)
#### [1.6. Unidades de información](UD01/1.6.Unidades_de_informacion.md)
#### [1.7. Operacións lóxicas](UD01/1.7.Operacions_loxicas.md)





